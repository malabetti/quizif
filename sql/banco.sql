create database quizif;

use quizif;

create table questoes (
    id int(11) primary key auto_increment,
    titulo varchar(100),
    descricao varchar(1024),
    disciplina varchar(50),
    dificuldade varchar(50),
    opcaoa varchar(100),
    opcaob varchar(100),
    opcaoc varchar(100),
    opcaod varchar(100),
    opcaoe varchar(100),
    opcaocorreta varchar(1),
    timestamp timestamp default current_timestamp
);

show tables;

select * from questoes;